<?php

/*-- - - - - - - - - - - - - - - - - - - - - -
  -
  -    keep_bet.php
  -
  -  Fichero que se encarga de guardar una
  -  apuesta aun no realizada en la variable
  -  sesion del usuario
  -
  -  Autores: Mateo Fdez de Gamboa Santiuste
  -           Pinar Sanz Sacristan
  -
  - - - - - - - - - - - - - - - - - - - - - -*/

  session_start();
  if (!isset($_SESSION['bets'])) {
    $_SESSION['bets'] = array();
  }

  array_push($_SESSION['bets'], array($_REQUEST['bet_id'], $_REQUEST['bet_amount'], $_REQUEST['bet_for']));

  if (!isset($_SESSION['bets'])) {
    $_SESSION['to_pay'] = $_REQUEST['bet_amount'];
  } else
    $_SESSION['to_pay'] = $_SESSION['to_pay'] + $_REQUEST['bet_amount'];

  header("Location: cart.php");
  exit();
?>