<?php

/*-- - - - - - - - - - - - - - - - - - - - - -
  -
  -    login.php
  -
  -  Fichero que se encarga del login de un
  -  usuario verificando si este esta o no
  -  previamente registrado
  -
  -  Autores: Mateo Fdez de Gamboa Santiuste
  -           Pinar Sanz Sacristan
  -
  - - - - - - - - - - - - - - - - - - - - - -*/

  session_start();
  $user = $_REQUEST["user"]; //recoger datos de usuario
  $password = $_REQUEST["password"]; //recoger datos de contraseña

  if ($user == null || $user == "" || $password == null || $password == "")
    echo "ERROR";
  if (file_exists("usuarios/".$user) && is_dir("usuarios/".$user)) {

    $file = fopen('usuarios/'.$user.'/datos.dat', 'r+');
    fscanf($file, "%s\n", $user2);
    fscanf($file, "%s\n", $password2);
    $pattern = array("\r\n", "\n", "\r");
    $password2 = str_replace($pattern, '', $password2);

    if (strcmp($password, $password2)) {
      echo "ERROR";
    } else {
      session_start();
      $_SESSION['user'] = $user;
      $_SESSION['password'] = $password;
      fscanf($file, "%s\n", $_SESSION['name']);
      fscanf($file, "%s\n", $_SESSION['surname']);
      fscanf($file, "%s\n", $_SESSION['birthday']);
      fscanf($file, "%s\n", $_SESSION['mail']);
      fscanf($file, "%s\n", $_SESSION['credit_card']);
      fscanf($file, "%s\n", $_SESSION['credit_card_balance']);
      fscanf($file, "%s\n", $_SESSION['card_expiration']);

      echo "OK";
    }
    fclose($file);
  } else
    echo "ERROR";
?>