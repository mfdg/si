<?php

/*-- - - - - - - - - - - - - - - - - - - - - -
  -
  -    edit_bet.php
  -
  -  Fichero que se encarga de editar la cantidad
  -  a apostar en una apuesta aun no realizada
  -  modificando su valor en la sesion del usuario
  -
  -  Autores: Mateo Fdez de Gamboa Santiuste
  -           Pinar Sanz Sacristan
  -
  - - - - - - - - - - - - - - - - - - - - - -*/

  session_start();

  $_SESSION['to_pay'] = $_SESSION['to_pay'] - $_SESSION['bets'][$_REQUEST['index']][1] + $_REQUEST['amount'];

  $_SESSION['bets'][$_REQUEST['index']][1] = $_REQUEST['amount'];

  header("Location: cart.php");
  exit();
?>