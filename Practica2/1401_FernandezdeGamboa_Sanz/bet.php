<!DOCTYPE html>

<!-- - - - - - - - - - - - - - - - - - - - - -
  -
  -    bet.php
  -
  -  Pagina de informacion de una apuesta
  -
  -  Autores: Mateo Fdez de Gamboa Santiuste
  -           Pinar Sanz Sacristan
  -
  - - - - - - - - - - - - - - - - - - - - - -->

<html>
<head>
  <title>BETSY apuestas</title>
  <link rel="stylesheet" type="text/css" href="styles.css"/>
  <script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js"></script>
  <script type="text/javascript" src="behavior.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js" ></script>
  <meta charset="UTF-8">

  <link rel="shortcut icon" type="image/x-ico" href="img/favicon.ico"/>
</head>

<body>
  <header>
    <img id="betsy_girl" alt="girl" src="img/betsy_girl.png"/>
    <a href="index.php"><p id="betsy_title">BETSY</p></a>

    <?php
      session_start();
      if (isset($_SESSION['user'])) {
        setcookie('user', $_SESSION['user']);
    ?>

      <div id="logged_message">
        <p>Bienvenido, <a class="link" href="profile.php"><b><?php echo $_SESSION['name']; ?></b></a></p>
        <p>
          <a class="white_button" href="logout.php">Cerrar sesi&oacute;n</a>
        </p>
      </div>

    <?php
      } else {
    ?>

      <form name="datos" id="login">
        <?php
        if (isset($_COOKIE['user']))
          echo '<input id="user_field" type="text" name="user" value="'.$_COOKIE['user'].'" required>';
        else
          echo '<input id="user_field" type="text" name="user" placeholder="Usuario" required>';
        ?>
        <input id="password_field" type="password" name="password" placeholder="Contrase&ntilde;a" required><br/>
        <div id="error_login">Error en usuario o contrase&ntilde;a</div>
        <div id="reminder_login">
          &iquest;A&uacute;n no tienes cuenta? <a class="link" href="sign_up.php">&iexcl;Reg&iacute;strate!</a>
        </div>
        <input id="login_button" class="white_button" type="button" value="Entrar" onclick="send_login()">
      </form>

    <?php
      }
    ?>

  <a href="cart.php" id="betsy_cart" class="cart_icon" title="Cart"><p id="betsy_cart_counter"><?php 
                                                        if (isset($_SESSION['bets']))
                                                          echo count($_SESSION['bets']);
                                                        else echo '0';
                                                      ?></p></a>

  </header>

  <div id="lateral_menu">
    <hr/>
    <div <?php if (!strcmp($get_sport, '')) echo "class='menu_selected'"; ?>><a href="index.php">
    <img class="icon" alt="home" src="img/home.png"/>Inicio</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'baloncesto')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=baloncesto">
    <img class="icon" alt="basket" src="img/basket.png"/>Baloncesto</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'ciclismo')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=ciclismo">
    <img class="icon" alt="bike" src="img/bike.png"/>Ciclismo</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'formula1')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=formula1">
    <img class="icon" alt="formula1" src="img/formula1.png"/>F&oacute;rmula 1</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'futbol')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=futbol">
    <img class="icon" alt="football" src="img/football.png"/>F&uacute;tbol</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'golf')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=golf">
    <img class="icon" alt="golf" src="img/golf.png"/>Golf</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'hockey')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=hockey">
    <img class="icon" alt="hockey" src="img/hockey.png"/>Hockey</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'rugby')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=rugby">
    <img class="icon" alt="rugby" src="img/rugby.png"/>Rugby</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'tenis')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=tenis">
    <img class="icon" alt="tennis" src="img/tennis.png"/>Tenis</a></div>
    <hr/>
  </div>

  <div id="main">

  <?php
    $our_bet = "";
    $xml=simplexml_load_file("bets.xml") or die("Error: Cannot create object");

    $id = $_REQUEST['id'];

    foreach ($xml->children() as $sport) {
      foreach($sport->children() as $bet) {
        if (!strcmp($bet["id"], $id)) {
          $our_bet = $bet;
          break;
        }
      }
      if (strcmp($our_bet, "")) {
        $our_sport = $sport;
        break;
      }
    }

    $team = 0;
    foreach ($our_bet->children() as $key) {
      if (strpos($key->getName(),'#') === false) {
        if (!strcmp($key->getName(), "rates")) {
          $i = 0;
          foreach ($key->children() as $rates) {
            if (strpos($rates->getName(),'#') === false) {
              $rate[$i] = (string)$rates;
              $i++;
            }
          }
        } else if (strcmp($key->getName(), "date")) {

          if (!strcmp($our_sport->getName(), "baloncesto") || !strcmp($our_sport->getName(), "futbol") ||
              !strcmp($our_sport->getName(), "hockey") || !strcmp($our_sport->getName(), "rugby") ||
              !strcmp($our_sport->getName(), "tenis")) {
            $is_comp = 0;
            if ($team == 0) {
              $title = (string)$key;
              $team += 1;
            } else
              $title = $title." - ".(string)$key;
          } else if (!strcmp($our_sport->getName(), "ciclismo") ||
                     !strcmp($our_sport->getName(), "formula1") ||
                     !strcmp($our_sport->getName(), "golf")) {
            $is_comp = 1;
            $title = (string)$key;
          }

        }
      }
    }

    $tie = 1;
    if(!$is_comp) {
      if (!strcmp($our_sport->getName(), "baloncesto") || !strcmp($our_sport->getName(), "tenis"))
        $tie = 0;
    }

    echo "<div class='container'><h2 id='bet_title'>".$title."</h2>";

    if ($is_comp) {
  ?>

      <form class="forms_margin">
        <fieldset>
          <legend><?php $i = 0; echo $rate[$i]; ?></legend>
          <label class="bet_label">Apuesta:</label>
          <input id="comp1" type="text" name="inlocal" placeholder="Apuesta"/>&euro;
          <a href='javascript:send_post("keep_bet.php", {id: "comp1", bet_id: "<?php echo $id; ?>", bet_for: "<?php echo $i; ?>"});'>
            <img class="icon" alt="accept" src="img/accept.png"/>
          </a>
        </fieldset>
      </form>
      <form class="forms_margin">
        <fieldset>
          <legend><?php $i += 1; echo $rate[$i]; ?></legend>
          <label class="bet_label">Apuesta:</label>
          <input id="comp2" type="text" name="indraw" placeholder="Apuesta"/>&euro;
          <a href='javascript:send_post("keep_bet.php", {id: "comp2", bet_id: "<?php echo $id; ?>", bet_for: "<?php echo $i; ?>"});'>
            <img class="icon" alt="accept" src="img/accept.png"/>
          </a>
        </fieldset>
      </form>
      <form class="margin_bottom forms_margin">
        <fieldset>
          <legend><?php $i += 1; echo $rate[$i]; ?></legend>
          <label class="bet_label">Apuesta:</label>
          <input id="comp3" type="text" name="inaway" placeholder="Apuesta"/>&euro;
          <a href='javascript:send_post("keep_bet.php", {id: "comp3", bet_id: "<?php echo $id; ?>", bet_for: "<?php echo $i; ?>"});'>
            <img class="icon" alt="accept" src="img/accept.png"/>
          </a>
        </fieldset>
      </form>
    </div>

  <?php
    } else {
  ?>

      <form class="forms_margin">
        <fieldset>
          <legend>Gana local</legend>
          <label class="bet_label">Cuota <?php $i = 0; echo $rate[$i]; ?></label>
          <input id="match1" type="text" name="inlocal" placeholder="Apuesta"/>&euro;
          <a href='javascript:send_post("keep_bet.php", {id: "match1", bet_id: "<?php echo $id; ?>", bet_for: "<?php echo $i; ?>"});'>
            <img class="icon" alt="accept" src="img/accept.png"/>
          </a>
        </fieldset>
      </form>

      <?php
        if ($tie) {
      ?>

        <form class="forms_margin">
          <fieldset>
            <legend>Empate</legend>
            <label class="bet_label">Cuota <?php $i += 1; echo $rate[$i]; ?></label>
            <input id="match2" type="text" name="indraw" placeholder="Apuesta"/>&euro;
            <a href='javascript:send_post("keep_bet.php", {id: "match2", bet_id: "<?php echo $id; ?>", bet_for: "<?php echo $i; ?>"});'>
            <img class="icon" alt="accept" src="img/accept.png"/>
          </a>
          </fieldset>
        </form>

      <?php
        }
      ?>

      <form class="margin_bottom forms_margin">
        <fieldset>
          <legend>Gana visitante</legend>
          <label class="bet_label">Cuota <?php $i += 1; echo $rate[$i]; ?></label>
          <input id="match3" type="text" name="inaway" placeholder="Apuesta"/>&euro;
          <a href='javascript:send_post("keep_bet.php", {id: "match3", bet_id: "<?php echo $id; ?>", bet_for: "<?php echo $i; ?>"});'>
            <img class="icon" alt="accept" src="img/accept.png"/>
          </a>
        </fieldset>
      </form>
    </div>
  <?php
    }
  ?>
  </div>

  <footer>
    <p id="footer_text">&copy;2015 BETSY apuestas deportivas - Escuela Polit&eacute;cnica Superior UAM</p>
  </footer>

</body>

<script type="text/javascript">
  $(function() {
    $("form input").keypress(function(e) {
      if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
        if ($("#password_field").is(":focus") || $("#user_field").is(":focus")) {
          $("#login_button").click();
          return false;
        }
        return false;
      } else
        return true;
    });
  });
</script>

</html>
