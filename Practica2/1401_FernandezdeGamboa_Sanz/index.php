<!DOCTYPE html>

<!-- - - - - - - - - - - - - - - - - - - - - -
  -
  -    index.php
  -
  -  Pagina principal de la web de apuestas BETSY
  -
  -  Autores: Mateo Fdez de Gamboa Santiuste
  -           Pinar Sanz Sacristan
  -
  - - - - - - - - - - - - - - - - - - - - - -->

<html>
<head>
  <title>BETSY apuestas</title>
  <link rel="stylesheet" type="text/css" href="styles.css"/>
  <script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js"></script>
  <script type="text/javascript" src="behavior.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js" ></script>
  <meta charset="UTF-8">

  <link rel="shortcut icon" type="image/x-ico" href="img/favicon.ico"/>
  <!--link rel="stylesheet" href="http://cdn.datatables.net/1.10.0/css/jquery.dataTables.css"/>  <!- Data tables' style ->
  <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <script src="http://cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>            <!- Data tables -->

</head>

<body>
  <header>

    <img id="betsy_girl" alt="girl" src="img/betsy_girl.png"/>
    <a href="index.php"><p id="betsy_title">BETSY</p></a>

    <?php
      session_start();
      if (isset($_SESSION['user'])) {
        setcookie('user', $_SESSION['user']);
    ?>

      <div id="logged_message">
        <p>Bienvenido, <a class="link" href="profile.php"><b><?php echo $_SESSION['name']; ?></b></a></p>
        <p>
          <a class="white_button" href="logout.php">Cerrar sesi&oacute;n</a>
        </p>
      </div>

    <?php
      } else {
    ?>

      <form name="datos" id="login">
        <?php
        if (isset($_COOKIE['user']))
          echo '<input id="user_field" type="text" name="user" value="'.$_COOKIE['user'].'" required>';
        else
          echo '<input id="user_field" type="text" name="user" placeholder="Usuario" required>';
        ?>
        <input id="password_field" type="password" name="password" placeholder="Contrase&ntilde;a" required><br/>
        <div id="error_login">Error en usuario o contrase&ntilde;a</div>
        <div id="reminder_login">
          &iquest;A&uacute;n no tienes cuenta? <a class="link" href="sign_up.php">&iexcl;Reg&iacute;strate!</a>
        </div>
        <input id="login_button" class="white_button" type="button" value="Entrar" onclick="send_login()">
      </form>

    <?php
      }
    ?>

  <a href="cart.php" id="betsy_cart" class="cart_icon" title="Cart"><p id="betsy_cart_counter"><?php 
                                                        if (isset($_SESSION['bets']))
                                                          echo count($_SESSION['bets']);
                                                        else echo '0';
                                                      ?></p></a>

  </header>

  <?php $get_sport = $_REQUEST['deporte']; ?>

  <div id="lateral_menu">
    <hr/>
    <div <?php if (!strcmp($get_sport, '')) echo "class='menu_selected'"; ?>><a href="index.php">
    <img class="icon" alt="home" src="img/home.png"/>Inicio</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'baloncesto')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=baloncesto">
    <img class="icon" alt="basket" src="img/basket.png"/>Baloncesto</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'ciclismo')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=ciclismo">
    <img class="icon" alt="bike" src="img/bike.png"/>Ciclismo</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'formula1')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=formula1">
    <img class="icon" alt="formula1" src="img/formula1.png"/>F&oacute;rmula 1</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'futbol')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=futbol">
    <img class="icon" alt="football" src="img/football.png"/>F&uacute;tbol</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'golf')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=golf">
    <img class="icon" alt="golf" src="img/golf.png"/>Golf</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'hockey')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=hockey">
    <img class="icon" alt="hockey" src="img/hockey.png"/>Hockey</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'rugby')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=rugby">
    <img class="icon" alt="rugby" src="img/rugby.png"/>Rugby</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'tenis')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=tenis">
    <img class="icon" alt="tennis" src="img/tennis.png"/>Tenis</a></div>
    <hr/>
  </div>


  <div id="main">

    <div class="select_style">
      <select id="category_select" class="black_select">
        <option value="ninguna" <?php if($_REQUEST['category'] == null) echo 'selected'; ?>>Categor&iacute;a</option>
        <option value="hoy" <?php if($_REQUEST['category'] == 'hoy') echo 'selected'; ?>>Hoy</option>
        <option value="top10" <?php if($_REQUEST['category'] == 'top10') echo 'selected'; ?>>Top 10</option>
        <option value="menor2" <?php if($_REQUEST['category'] == 'menor2') echo 'selected'; ?>>Cuota menor a 2&#8364;</option>
        <option value="mayor5" <?php if($_REQUEST['category'] == 'mayor5') echo 'selected'; ?>>Cuota mayor a 5&#8364;</option>
      </select>
    </div>

    <div class="searchbar_style">
      <form><!-- action="#" onsubmit="return search();"-->
        <input id="searchbar_text" class="text_input" type="search" placeholder="Buscar..."/>
        <input id="search_button" type="button" class="black_button" value="Buscar"/><!--type="submit"-->
      </form>
    </div>

    <div id="table_block">
    <?php
      $x = simplexml_load_file("bets.xml") or die("Error: Cannot create object");
      $id = 0;
      foreach ($x->children() as $sport) {
        $is_table = 0;
        $table_buffer = "";

        $get_search = $_REQUEST['search'];
        $get_category = $_REQUEST['category'];
        if ($get_sport == null || !strcmp($get_sport, $sport->getName())) {
          if (strpos($sport->getName(), '#') === false) {
            $table_buffer = $table_buffer."<table class='bordered'>
                                            <caption>".ucfirst($sport->getName())."</caption>
                                            <tbody>";

            foreach ($sport->children() as $tag) {

              if (strpos($tag->getName(),'#') === false)
                $id = $tag["id"];

              if (!strcmp($tag->getName(), "bet")) {
                $table_buffer = $table_buffer;
                $entry_buffer = "<tr>";

                $is_key = 0;
                if ($get_search == null || $get_search == "" || 
                    strpos(strtolower($sport->getName()), strtolower($get_search)) !== false) {
                  $is_key = 1;
                }

                $cat_ok = 0;
                if ($get_category == null || $get_category == "") {
                  $cat_ok = 1;
                }

                foreach ($tag->children() as $key) {

                  if (strpos($key->getName(),'#') === false) {

                    if (!strcmp($key->getName(), "rates")) {
                      foreach ($key->children() as $rate) {
                        if (strpos($rate->getName(),'#') === false) {
                          if ($cat_ok == 0) {
                            if (strpos($rate, "-") !== false) {
                              if (!strcmp($get_category, "menor2") &&
                                floatval(substr($rate, strpos($rate, "-")+2)) < 2.0) {
                                $cat_ok = 1;
                              } else if (!strcmp($get_category, "mayor5") &&
                                floatval(substr($rate, strpos($rate, "-")+2)) > 5.0) {
                                $cat_ok = 1;
                              }
                            } else {
                              if (!strcmp($get_category, "menor2") &&
                                  floatval($rate) < 2.0) {
                                $cat_ok = 1;
                              } else if (!strcmp($get_category, "mayor5") &&
                                          floatval($rate) > 5.0) {
                                $cat_ok = 1;
                              }
                            }
                          }
                          if ($is_key == 0 && strpos(strtolower($rate), strtolower($get_search)) !== false) {
                            $is_key = 1;
                          }
                          
                          $entry_buffer = $entry_buffer."<td><a href='bet.php?id=$id'>$rate</a></td>";
                        }
                      }
                    } else {
                      if ($is_key == 0 && strpos(strtolower($key), strtolower($get_search)) !== false) {
                        $is_key = 1;
                      }
                      $entry_buffer = $entry_buffer."<td><a href='bet.php?id=$id'>$key</a></td>";
                    }
                  }
                }
                if ($is_key == 1 && $cat_ok == 1) {    /*FILTRADO*/
                  $is_table = 1;
                  $table_buffer = $table_buffer.$entry_buffer;
                }

                $table_buffer = $table_buffer."</tr>";
              }


              if (!strcmp($tag->getName(), "title")) {
                $table_buffer = $table_buffer."<thead><tr>";
                $colspan = 1;
                $class = "";
                foreach ($tag->children() as $key) {
                  if (strpos($key->getName(),'#') === false) {
                    // if (!strcmp($key, $key->nextSibling->nodeValue)) {
                    //   $colspan += 1;
                    // } else {
                      $table_buffer = $table_buffer."<th colspan='$colspan' class='$class'>$key</th>";
                      $colspan = 1;
                    // }
                  }
                }
                $table_buffer = $table_buffer."</tr></thead>";
              }
            }
          }

          if (strpos($sport->getName(),'#') === false) {
            $table_buffer = $table_buffer."</tbody>
                                            </table>
                                            <hr class='tableline'/>";
          }
        }
      if ($is_table == 1)
        echo $table_buffer;
      }
    ?>
    </div>

    <div id="footer2" class="margin_bottom">
      <p><a class="link2" href="">&iquest;Qui&eacute;nes somos?</a> | <a class="link2" href="">Pol&iacute;tica de privacidad</a> | <a class="link2" href="">Preguntas frecuentes</a></p>
      <div class="social_icon_block">
        <a href="http://twitter.com/" id="twitter" class="social_icon" title="Twitter link"></a>
        <a href="http://facebook.com/" id="facebook" class="social_icon" title="Facebook link"></a>
        <a href="http://linkedin.com/" id="linkedin" class="social_icon" title="LinkedIn link"></a>
      </div>
    </div>

  </div>

  <footer>
    <p id="footer_text">&copy;2015 BETSY apuestas deportivas - Escuela Polit&eacute;cnica Superior UAM</p>
  </footer>

</body>

<script type="text/javascript">
  $('#category_select').change(function() {
    var category = $("#category_select option:selected").val();
    if (category == 'ninguna') {
      window.location = "index.php";
    } else {
      var url = window.location.href.substr(window.location.href.lastIndexOf("/")+1);
      if (window.location.href.indexOf("deporte") > -1) {
        window.location = url + "&category=" + category;
      } else
        window.location = "index.php?category=" + category;
      $('#category_select').val(category);
    }
  });

  $("#search_button").click(function() {
    var select = document.getElementById("category_select");
    var category = select.options[select.selectedIndex].value;
    var searchbar = document.getElementById("searchbar_text").value;
    var url = window.location.href.substr(window.location.href.lastIndexOf("/")+1);
    if (searchbar == null || searchbar == "") {
      if (category != "ninguna") {
        if (window.location.href.indexOf("deporte") > -1) {
          window.location = url + "&category=" + category;
        } else {
          window.location = "index.php?category=" + category;
        }
      } else {
        return false;
      }
    } else {
      if (category != "ninguna") {
        if (window.location.href.indexOf("deporte") > -1) {
          window.location = url + "&search=" + searchbar + "&category=" + category;
        } else {
          window.location = "index.php?search=" + searchbar + "&category=" + category;
        }
      } else {
        if (window.location.href.indexOf("deporte") > -1) {
          window.location = url + "&search=" + searchbar;
        } else {
          window.location = "index.php?search=" + searchbar;

        }
      }
    }
  });
</script>

<script type="text/javascript">
  $(function() {
    $("form input").keypress(function(e) {
      if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
        if ($("#password_field").is(":focus") || $("#user_field").is(":focus")) {
          $("#login_button").click();
          return false;
        } else if ($("#searchbar_text").is(":focus")) {
          $("#search_button").click();
          return false;
        }
        return true;
      } else
        return true;
    });
  });
</script>

</html>
