<?php

/*-- - - - - - - - - - - - - - - - - - - - - -
  -
  -    cart_payment.php
  -
  -  Fichero que se encarga de realizar el
  -  pago del carrito de apuestas
  -
  -  Autores: Mateo Fdez de Gamboa Santiuste
  -           Pinar Sanz Sacristan
  -
  - - - - - - - - - - - - - - - - - - - - - -*/

  session_start();

  $_SESSION['credit_card_balance'] = $_SESSION['credit_card_balance'] - $_SESSION['to_pay'];
  $_SESSION['to_pay'] = 0;

  /* Guardar apuestas en el historial */
  $user = $_SESSION["user"]; //recoger datos de usuario

  if ($user == null || $user == "") {
    $_SESSION['error'] = "Error al verificar el usuario.";
    $_SESSION['return'] = "cart.php";
    header("Location: error.php");
    exit();
  }

  $file = simplexml_load_file('usuarios/'.$user.'/history.xml');

  foreach ($_SESSION["bets"] as $bet) {
    $child = $file->addChild("bet");
    $child->addChild("id", $bet[0]);
    $child->addChild("amount", $bet[1]);
    $child->addChild("for", $bet[2]);
  }

  $file->asXML('usuarios/'.$user.'/history.xml');

  $file = fopen("usuarios/".$_SESSION['user']."/datos.dat", 'w');
  fwrite($file, $_SESSION['user']."\n".$_SESSION['password']."\n".$_SESSION['name']."\n".$_SESSION['surname']."\n".$_SESSION['birthday']."\n".$_SESSION['mail']."\n".$_SESSION['credit_card']."\n".$_SESSION['credit_card_balance']."\n".$_SESSION['card_expiration']);
  fclose($file);

  /* Mensaje de apuesta realizada correctamente */

  unset($_SESSION['bets']);
  header("Location: history.php");
  exit();
?>