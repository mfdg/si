<?php

/*-- - - - - - - - - - - - - - - - - - - - - -
  -
  -    remove_bet.php
  -
  -  Fichero que se encarga de eliminar una
  -  apuesta del carrito de apuestas
  -
  -  Autores: Mateo Fdez de Gamboa Santiuste
  -           Pinar Sanz Sacristan
  -
  - - - - - - - - - - - - - - - - - - - - - -*/

  session_start();

  $_SESSION['to_pay'] = $_SESSION['to_pay'] - $_SESSION['bets'][$_REQUEST['index']][1];

  unset($_SESSION['bets'][$_REQUEST['index']]);

  $_SESSION['bets'] = array_values($_SESSION['bets']);

  header("Location: cart.php");
  exit();
?>