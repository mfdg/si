<!DOCTYPE html>

<!-- - - - - - - - - - - - - - - - - - - - - -
  -
  -    history.php
  -
  -  Pagina donde se consulta el historial de apuestas
  -
  -  Autores: Mateo Fdez de Gamboa Santiuste
  -           Pinar Sanz Sacristan
  -
  - - - - - - - - - - - - - - - - - - - - - -->

<html>
<head>
  <title>BETSY apuestas</title>
  <link rel="stylesheet" type="text/css" href="styles.css"/>
  <meta charset="UTF-8">

  <link rel="shortcut icon" type="image/x-ico" href="img/favicon.ico"/>
</head>

<body>
  <header>
    <img id="betsy_girl" alt="girl" src="img/betsy_girl.png"/>
    <a href="index.php"><p id="betsy_title">BETSY</p></a>

    <?php session_start(); ?>

    <div id="logged_message">
      <p>Bienvenido, <a class="link" href="profile.php"><b><?php echo $_SESSION['name']; ?></b></a></p>
      <p>
        <a class="white_button" href="logout.php">Cerrar sesi&oacute;n</a>
      </p>
    </div>

  <a href="cart.php" id="betsy_cart" class="cart_icon" title="Cart"><p id="betsy_cart_counter"><?php 
                                                        if (isset($_SESSION['bets']))
                                                          echo count($_SESSION['bets']);
                                                        else echo '0';
                                                      ?></p></a>

  </header>

  <div id="lateral_menu">
    <hr/>
    <div <?php if (!strcmp($get_sport, '')) echo "class='menu_selected'"; ?>><a href="index.php">
    <img class="icon" alt="home" src="img/home.png"/>Inicio</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'baloncesto')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=baloncesto">
    <img class="icon" alt="basket" src="img/basket.png"/>Baloncesto</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'ciclismo')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=ciclismo">
    <img class="icon" alt="bike" src="img/bike.png"/>Ciclismo</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'formula1')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=formula1">
    <img class="icon" alt="formula1" src="img/formula1.png"/>F&oacute;rmula 1</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'futbol')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=futbol">
    <img class="icon" alt="football" src="img/football.png"/>F&uacute;tbol</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'golf')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=golf">
    <img class="icon" alt="golf" src="img/golf.png"/>Golf</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'hockey')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=hockey">
    <img class="icon" alt="hockey" src="img/hockey.png"/>Hockey</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'rugby')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=rugby">
    <img class="icon" alt="rugby" src="img/rugby.png"/>Rugby</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'tenis')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=tenis">
    <img class="icon" alt="tennis" src="img/tennis.png"/>Tenis</a></div>
    <hr/>
  </div>

  <div id="main">

<?php
    $our_bet = "";
    $flag = 0;
    $tie = 1;
    $index = 0;

    $xml = simplexml_load_file("bets.xml") or die("Error: Cannot create object 1");

    $history = simplexml_load_file('usuarios/'.$_SESSION['user'].'/history.xml');// or die("Error: Cannot create object 2");

    if (count($history->children()) == 0) {
  ?>

    <div>
      <h2>&iexcl;Tu historial est&aacute; vac&iacute;o!</h2>
      <p>&iexcl;Comienza a ganar dinero con nosotros <b><a href="index.php" class="link2">apostando</a></b> en BETSY!</p>
    </div>

  <?php
    } else {
    $comp = array();
    $match = array();
    foreach ($history->children() as $hbet) {
      if (strpos($hbet->getName(),'#') === false) {
        foreach ($hbet->children() as $hkey) {
          if (strpos($hbet->getName(),'#') === false) {
            if (!strcmp($hkey->getName(), "id"))
              $id = $hkey;
            else if (!strcmp($hkey->getName(), "amount"))
              $amount = $hkey;
            else
              $for = $hkey;
          }
        }
      }

      foreach ($xml->children() as $sport) {
        foreach($sport->children() as $bet) {
          if (!strcmp($bet["id"], $id)) {
            $flag = 1;
            $our_bet = $bet;
            break;
          }
        }
        if (strcmp($our_bet, "")) {
          $our_sport = $sport;
          if (!strcmp($our_sport->getName(), "baloncesto") || !strcmp($our_sport->getName(), "tenis"))
            $tie = 0;
          break;
        }
      }

      if ($flag) {
        $j = 0;
        foreach ($our_bet->children() as $key) {
          if (strpos($key->getName(),'#') === false) {
            if (!strcmp($key->getName(), "rates")) {
              $i = 0;
              foreach ($key->children() as $rates) {
                if (strpos($rates->getName(),'#') === false) {
                  $rate[$i] = (string)$rates; // Rates
                  $i++;
                }
              }
            } else if (!strcmp($key->getName(), "date")) {
              $date = (string)$key; // Date
            } else {
              if (!strcmp($our_sport->getName(), "baloncesto") || !strcmp($our_sport->getName(), "futbol") ||
                  !strcmp($our_sport->getName(), "hockey") || !strcmp($our_sport->getName(), "rugby") ||
                  !strcmp($our_sport->getName(), "tenis")) {
                $is_comp = 0;
                $team[$j] = (string)$key;
                $j++;
              } else if (!strcmp($our_sport->getName(), "ciclismo") ||
                         !strcmp($our_sport->getName(), "formula1") ||
                         !strcmp($our_sport->getName(), "golf")) {
                $is_comp = 1;
                $title = (string)$key;
              }

            }
          }
        }

        if ($is_comp) {
          $row = "<tr>
            <td>$title</td>
            <td>".$rate[(int)$for]."</td>
            <td>$amount &euro;</td>
            <td>$date</td>
            </tr>";
          array_push($comp, $row);
        } else {
          if ($for == 0)
            $winner = '1';
          else if ($for == 1)
            if (tie == 1)
              $winner = 'X';
            else
              $winner = '2';
          else
            $winner = '2';

          $row = "<tr>
            <td>$team[0]</td>
            <td>$team[1]</td>
            <td>$winner</td>
            <td>".$rate[(int)$for]."</td>
            <td>$amount &euro;</td>
            <td>$date</td>
            </tr>";
          array_push($match, $row);
        }
        $our_bet = "";
        $flag = 0;
      }
      $tie = 1;
      $index++;
    }

    echo "<h1 class='cart_title'>Apuestas realizadas</h1>";

    if (count($match) != 0) {
  ?>

    <table class="bordered">
      
      <thead>
        <tr>
          <th colspan="2" class="ul_corner">Partido</th>
          <th>Apuesta</th>
          <th>Cuota</th>
          <th>Dinero</th>
          <th class="ur_corner border_right">Fecha</th>
        </tr>
      </thead>

      <tbody>
      <?php
        foreach ($match as $bet) {
          echo $bet;
        }
      ?>
        <tr>
        </tr>

      </tbody>
    </table>

    <?php
      }

      if (count($comp) != 0) {
    ?>

    <table class="bordered">
      
      <thead>
        <tr>
          <th class="ul_corner">Competici&oacute;n</th>
          <th>Cuota</th>
          <th>Dinero</th>
          <th class="ur_corner border_right">Fecha</th>
        </tr>
      </thead>

      <tbody>
      
      <?php
        foreach ($comp as $bet) {
          echo $bet;
        }
      ?>
        <tr>
        </tr>

      </tbody>
    </table><br/>

    <?php
        }
      }
    ?>

    <div id="history_buttons">
      <a class="black_button" href="profile.php">Volver</a>
    </div>

  </div>

  <footer>
    <p id="footer_text">&copy;2015 BETSY apuestas deportivas - Escuela Polit&eacute;cnica Superior UAM</p>
  </footer>

</body>

</html>