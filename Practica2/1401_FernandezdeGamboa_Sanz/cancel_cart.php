<?php

/*-- - - - - - - - - - - - - - - - - - - - - -
  -
  -    cancel_cart.php
  -
  -  Fichero que se encarga de cancelar una
  -  apuesta
  -
  -  Autores: Mateo Fdez de Gamboa Santiuste
  -           Pinar Sanz Sacristan
  -
  - - - - - - - - - - - - - - - - - - - - - -*/

  session_start();
  unset($_SESSION['bets']);

  $_SESSION['bets'] = array_values($_SESSION['bets']);

  header("Location: index.php");
  exit();
?>