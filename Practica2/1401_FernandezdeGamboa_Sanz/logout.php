<?php

/*-- - - - - - - - - - - - - - - - - - - - - -
  -
  -    logout.php
  -
  -  Fichero que se encarga de cerrar la
  -  sesion de un usuario
  -
  -  Autores: Mateo Fdez de Gamboa Santiuste
  -           Pinar Sanz Sacristan
  -
  - - - - - - - - - - - - - - - - - - - - - -*/


  session_start();
  session_destroy();
  header("Location: index.php");
  exit();
?>