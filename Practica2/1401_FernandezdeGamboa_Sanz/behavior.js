/* - - - - - - - - - - - - - - - - - - - - -
  -
  -    behavior.js
  -
  -  Fichero en el que se encuentran las
  -  funciones javascript
  -
  -  Autores: Mateo Fdez de Gamboa Santiuste
  -           Pinar Sanz Sacristan
  -
  - - - - - - - - - - - - - - - - - - - - - */

// Función creación del objeto XMLHttpRequest 
function createAjaxObject () {
  var obj;
  if (window.XMLHttpRequest) { //Mayoría de navegadores
    obj=new XMLHttpRequest();
  }
  else { //para IE 5 y IE 6
    obj=new ActiveXObject(Microsoft.XMLHTTP);
  }
  return obj;
}

// Función que envia una peticion al servidor
function send_login() {
  // Recoger datos del formulario:
  var user = document.datos.user.value;
  var password = CryptoJS.MD5(document.datos.password.value);
  // Datos para el envio por POST:
  data = "user="+user+"&password="+password;
  // Objeto XMLHttpRequest creado por la función.
  objetoAjax=createAjaxObject();
  // Preparar el envio con Open
  objetoAjax.open("POST","login.php",true);
  // Enviar cabeceras para que acepte POST:
  objetoAjax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  /*objetoAjax.setRequestHeader("Content-length", data.length); //unsafe
  objetoAjax.setRequestHeader("Connection", "close");  //unsafe */
  objetoAjax.onreadystatechange=receive_login;
  objetoAjax.send(data);
}

// Función que recibe la respuesta del servidor
function receive_login() {
  if (objetoAjax.readyState == 4 && objetoAjax.status == 200) {
    miTexto=objetoAjax.responseText;
  	if(miTexto == "OK") {
      window.location.reload();
  	} else {
      //console.log(miTexto);
      document.getElementById("error_login").style.visibility = "visible";
  	}
  }
}

function submit_signup() {
  var name_ip = document.getElementById("name").value;
  if (validate_signup()) {
    document.getElementById("signup").submit();
    return true;
  } else {
    return false;
  }
}

function validate_signup() {
  var ret = true;

  // Nombre
  var pattern = /^[A-Za-z]+$/;
  var name_ip = document.getElementById("name").value;
  if (name_ip == null || name_ip == "") {
    ret = false;
    $("#name").addClass("wrong");
  } else if (name_ip.length < 2 || !pattern.test(name_ip)) {
    ret = false;
    $("#name").addClass("wrong");
  } else
    $("#name").removeClass("wrong");


  // Apellido
  var surname_ip = document.getElementById("surname").value;
  if (surname_ip == null || surname_ip == "") {
    ret = false;
    $("#surname").addClass("wrong");
  } else if (surname_ip.length < 2 || !pattern.test(surname_ip)) {
    ret = false;
    $("#surname").addClass("wrong");
  } else
    $("#surname").removeClass("wrong");

  // Fecha de nacimiento
  var birth_day_ip = document.getElementById("birth_day").value;
  var birth_month_ip = document.getElementById("birth_month").value;
  var birth_year_ip = document.getElementById("birth_year").value;
  var current_day = new Date().getDate();
  var current_month = new Date().getMonth()+1;
  var current_year = new Date().getFullYear();
  if (birth_day_ip == "Día") {
    ret = false;
    $("#birth_day").addClass("wrong");
  } else if (birth_month_ip == "Mes") {
    ret = false;
    $("#birth_day").removeClass("wrong");
    $("#birth_month").addClass("wrong");
  } else if (birth_year_ip == "Año") {
    ret = false;
    $("#birth_day").removeClass("wrong");
    $("#birth_month").removeClass("wrong");
    $("#birth_year").addClass("wrong");
  } else if (birth_year_ip > (current_year - 18) ||
    (birth_year_ip == (current_year - 18) &&
      (birth_month_ip > current_month ||
        (birth_month_ip == current_month && birth_day_ip > current_day)))) {
    ret = false;
    $("#birth_day").addClass("wrong");
    $("#birth_month").addClass("wrong");
    $("#birth_year").addClass("wrong");
  } else {
    $("#birth_day").removeClass("wrong");
    $("#birth_month").removeClass("wrong");
    $("#birth_year").removeClass("wrong");
  }

  // Correo
  pattern = /^[A-Za-z][A-Za-z0-9|_|-|.]+@[A-Za-z][A-Za-z0-9|.]+.[A-Za-z]+$/;
  var mail_ip = document.getElementById("mail").value;
  if (mail_ip == null || mail_ip == "") {
    ret = false;
    $("#mail").addClass("wrong");
  } else if (mail_ip.length < 2 || !pattern.test(mail_ip)) {
    ret = false;
    $("#mail").addClass("wrong");
  } else
    $("#mail").removeClass("wrong");

  // Tarjeta de credito
  pattern = /^[0-9]{16}$/;
  var card_ip = document.getElementById("credit_card").value;
  if (card_ip == null || card_ip == "") {
    ret = false;
    $("#credit_card").addClass("wrong");
  } else if (!pattern.test(card_ip)) {
    ret = false;
    $("#credit_card").addClass("wrong");
  } else
    $("#credit_card").removeClass("wrong");

  // Caducidad
  var card_month_ip = document.getElementById("card_month").value;
  var card_year_ip = document.getElementById("card_year").value;
  if (card_month_ip == "Mes") {
    ret = false;
    $("#card_month").addClass("wrong");
  } else if (card_year_ip == "Año") {
    ret = false;
    $("#card_month").removeClass("wrong");
    $("#card_year").addClass("wrong");
  } else if (card_year_ip < current_year ||
    (card_year_ip == current_year && card_month_ip < current_month)) {
    ret = false;
    $("#card_month").addClass("wrong");
    $("#card_year").addClass("wrong");
  } else {
    $("#card_month").removeClass("wrong");
    $("#card_year").removeClass("wrong");
  }

  // Usuario
  var pattern = /^[A-Za-z]([A-Za-z0-9]+[_|-|.]?)+$/;
  var user_ip = document.getElementById("user").value;
  if (user_ip == null || user_ip == "") {
    ret = false;
    $("#user").addClass("wrong");
  } else if (user_ip.length < 2 || !pattern.test(user_ip)) {
    ret = false;
    $("#user").addClass("wrong");
  } else
    $("#user").removeClass("wrong");

  // Contraseña
  var pass_ip = document.getElementById("password").value;
  var pass2_ip = document.getElementById("password2").value;
  if (pass_ip == null || pass_ip == "") {
    ret = false;
    $("#password").addClass("wrong");
  } else if (pass2_ip == null || pass2_ip == "") {
    ret = false;
    $("#password").removeClass("wrong");
    $("#password2").addClass("wrong");
  } else if (pass_ip.length < 8) {
    ret = false;
    $("#password").addClass("wrong");
  } else if (pass_ip != pass2_ip) {
    ret = false;
    $("#password").addClass("wrong");
    $("#password2").addClass("wrong");
  } else {
    $("#password").removeClass("wrong");
    $("#password2").removeClass("wrong");
  }

  return ret;
}

function add_money() {
  var amount = prompt("Introduzca la cantidad deseada", "5");
    if (amount != null) {
      if (!isNaN(amount) && amount > 0) {
        window.location = "add_money.php?amount=" + amount;
      } else {
        alert("Debe introducir una cifra mayor que 0.");
      }
    }
}

function send_post(path, params, method) {
  method = method || "post"; // Set method to post by default if not specified.

  var form = document.createElement("form");
  form.setAttribute("method", method);
  form.setAttribute("action", path);

  for(var key in params) {
    if(params.hasOwnProperty(key)) {
      if (key == "id") {
        var amount = document.getElementById(params[key]).value;
        if (amount != null) {
          if (!isNaN(amount) && amount > 0) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", "bet_amount");
            hiddenField.setAttribute("value", amount);
          } else {
            alert("Debe introducir una cifra mayor que 0.");
            return;
          }
        } else {
            alert("Debe introducir una cifra mayor que 0.");
            return;
        }
      } else {
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", key);
        hiddenField.setAttribute("value", params[key]);
      }
      form.appendChild(hiddenField);
    }
  }

  document.body.appendChild(form);
  form.submit();
}

function cancel_cart() {
  if (confirm("Se eliminarán todas las apuestas del carrito\n¿Seguro que quiere continuar?") == true)
      window.location = "cancel_cart.php";
  else
      return;
}

function accept_cart(logged, wallet, to_pay) {

  if (logged == false) {
    if (confirm("Para realizar las apuestas del carrito tienes que estar registrado.\nSi ya posees una cuenta, inicia sesión.\nSi no, acepte para crear una cuenta nueva.") == true)
      window.location = "sign_up.php";
    else
      return;
  } else {
    if (wallet < to_pay)
      alert('No tienes suficiente dinero para realizar la operación. Haz una recarga desde tu perfil de usuario y vuelve a intentarlo.');
    else {
      if (confirm("Se le descontaran del saldo " + to_pay + " euros.") == true)
        window.location = "cart_payment.php";
      else
        return;
    }
  }
}

function edit_bet(id) {
  $("#"+id).html("<input id='in" + id + "' class='size1' type='text'></input>");
  $("#edit"+id).hide();
  $("#remove"+id).hide();
  $("#accept"+id).show();
  $("#cancel"+id).show();
}

function accept_edition(id) {
  window.location = "edit_bet.php?index=" + id + "&amount=" + document.getElementById("in" + id).value;
}

function cancel_edition(id) {
  window.location.reload();
}