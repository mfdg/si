<!DOCTYPE html>

<!-- - - - - - - - - - - - - - - - - - - - - -
  -
  -    profile.php
  -
  -  Pagina de perfil
  -
  -  Autores: Mateo Fdez de Gamboa Santiuste
  -           Pinar Sanz Sacristan
  -
  - - - - - - - - - - - - - - - - - - - - - -->

<html>
<head>
  <title>BETSY apuestas</title>
  <link rel="stylesheet" type="text/css" href="styles.css"/>
  <script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js"></script>
  <script type="text/javascript" src="behavior.js"></script>
  <meta charset="UTF-8">

  <link rel="shortcut icon" type="image/x-ico" href="img/favicon.ico"/>
</head>

<body>
  <header>
    <img id="betsy_girl" alt="girl" src="img/betsy_girl.png"/>
    <a href="index.php"><p id="betsy_title">BETSY</p></a>

    <?php session_start(); ?>

    <div id="logged_message">
      <p>Bienvenido, <a class="link" href="profile.php"><b><?php echo $_SESSION['name']; ?></b></a></p>
      <p>
        <a class="white_button" href="logout.php">Cerrar sesi&oacute;n</a>
      </p>
    </div>

  <a href="cart.php" id="betsy_cart" class="cart_icon" title="Cart"><p id="betsy_cart_counter"><?php 
                                                        if (isset($_SESSION['bets']))
                                                          echo count($_SESSION['bets']);
                                                        else echo '0';
                                                      ?></p></a>

  </header>


  <div id="lateral_menu">
    <hr/>
    <div <?php if (!strcmp($get_sport, '')) echo "class='menu_selected'"; ?>><a href="index.php">
    <img class="icon" alt="home" src="img/home.png"/>Inicio</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'baloncesto')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=baloncesto">
    <img class="icon" alt="basket" src="img/basket.png"/>Baloncesto</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'ciclismo')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=ciclismo">
    <img class="icon" alt="bike" src="img/bike.png"/>Ciclismo</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'formula1')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=formula1">
    <img class="icon" alt="formula1" src="img/formula1.png"/>F&oacute;rmula 1</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'futbol')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=futbol">
    <img class="icon" alt="football" src="img/football.png"/>F&uacute;tbol</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'golf')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=golf">
    <img class="icon" alt="golf" src="img/golf.png"/>Golf</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'hockey')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=hockey">
    <img class="icon" alt="hockey" src="img/hockey.png"/>Hockey</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'rugby')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=rugby">
    <img class="icon" alt="rugby" src="img/rugby.png"/>Rugby</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'tenis')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=tenis">
    <img class="icon" alt="tennis" src="img/tennis.png"/>Tenis</a></div>
    <hr/>
  </div>


  <div id="main">
    <div class="profile_content">
      <h2 class="profile_title"><?php echo $_SESSION['name']." ".$_SESSION['surname']; ?></h2>
      <div class="profile_content_row"><p>Usuario: <span class="profile_tag"><?php echo $_SESSION['user']; ?></span></p></div>
      <div class="profile_content_row"><p>Correo: <span class="profile_tag"><?php echo $_SESSION['mail']; ?></span></p></div>
      <div class="profile_content_row"><p>Fecha de nacimiento: <span class="profile_tag"><?php echo $_SESSION['birthday']; ?></span></p></div>
      <div class="profile_content_row"><p>Tarjeta de cr&eacute;dito: <span class="profile_tag"><?php echo $_SESSION['credit_card']; ?></span></p></div>
      <div class="profile_content_row"><p>Saldo: <span class="profile_tag"><?php echo $_SESSION['credit_card_balance']; ?> &euro;</span></p></div>
      <p class="profile_buttons"><a class="black_button" onclick="add_money()">Añadir dinero</a>
      <a class="black_button" href="history.php">Historial</a></p>
    </div>
  </div>

  <footer>
    <p id="footer_text">&copy;2015 BETSY apuestas deportivas - Escuela Polit&eacute;cnica Superior UAM</p>
  </footer>

</body>

</html>
