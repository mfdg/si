<!DOCTYPE html>

<!-- - - - - - - - - - - - - - - - - - - - - -
  -
  -    sign_up.php
  -
  -  Pagina de registro
  -
  -  Autores: Mateo Fdez de Gamboa Santiuste
  -           Pinar Sanz Sacristan
  -
  - - - - - - - - - - - - - - - - - - - - - -->

<html>
<head>
  <title>BETSY apuestas</title>
  <link rel="stylesheet" type="text/css" href="styles.css"/>
  <script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js"></script>
  <script type="text/javascript" src="behavior.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js" ></script>
  <meta charset="UTF-8">

  <link rel="shortcut icon" type="image/x-ico" href="img/favicon.ico"/>
</head>

<body>
  <header>
    <img id="betsy_girl" alt="girl" src="img/betsy_girl.png"/>
    <a href="index.php"><p id="betsy_title">BETSY</p></a>

    <?php session_start(); ?>

    <form name="datos" id="login">
      <?php
      if (isset($_COOKIE['user']))
        echo '<input id="user_field" type="text" name="user" value="'.$_COOKIE['user'].'" required>';
      else
        echo '<input id="user_field" type="text" name="user" placeholder="Usuario" required>';
      ?>
      <input id="password_field" type="password" name="password" placeholder="Contrase&ntilde;a" required><br/>
      <div id="error_login">Error en usuario o contrase&ntilde;a</div>
      <div id="reminder_login">
        &iquest;A&uacute;n no tienes cuenta? <a class="link" href="sign_up.php">&iexcl;Reg&iacute;strate!</a>
      </div>
      <input id="login_button" class="white_button" type="button" value="Entrar" onclick="send_login()">
    </form>


  <a href="cart.php" id="betsy_cart" class="cart_icon" title="Cart"><p id="betsy_cart_counter"><?php 
                                                        if (isset($_SESSION['bets']))
                                                          echo count($_SESSION['bets']);
                                                        else echo '0';
                                                      ?></p></a>

  </header>

  <div id="lateral_menu">
    <hr/>
    <div <?php if (!strcmp($get_sport, '')) echo "class='menu_selected'"; ?>><a href="index.php">
    <img class="icon" alt="home" src="img/home.png"/>Inicio</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'baloncesto')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=baloncesto">
    <img class="icon" alt="basket" src="img/basket.png"/>Baloncesto</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'ciclismo')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=ciclismo">
    <img class="icon" alt="bike" src="img/bike.png"/>Ciclismo</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'formula1')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=formula1">
    <img class="icon" alt="formula1" src="img/formula1.png"/>F&oacute;rmula 1</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'futbol')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=futbol">
    <img class="icon" alt="football" src="img/football.png"/>F&uacute;tbol</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'golf')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=golf">
    <img class="icon" alt="golf" src="img/golf.png"/>Golf</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'hockey')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=hockey">
    <img class="icon" alt="hockey" src="img/hockey.png"/>Hockey</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'rugby')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=rugby">
    <img class="icon" alt="rugby" src="img/rugby.png"/>Rugby</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'tenis')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=tenis">
    <img class="icon" alt="tennis" src="img/tennis.png"/>Tenis</a></div>
    <hr/>
  </div>

  <div id="main">

    <img id="betsy_guy" alt="guy" src="img/betsy_guy.png"/>

    <form id="signup" action="registration.php" method="post">
      <fieldset>
      <legend>Registro</legend>
        <label class="width1" for="name"><span class="form_bg">Nombre:</span></label>
        <input type="text" name="name" placeholder="Nombre" id="name" required/>
        <br/>
        <label class="width1" for="surname"><span class="form_bg">Apellidos:</span></label>
        <input type="text" name="surname" placeholder="Apellidos" id="surname" required/>
        <br/>
        <label class="width1" for="mail"><span class="form_bg">Correo:</span></label>
        <input type="text" name="mail" placeholder="Correo" id="mail" required/>
        <br/>
        <label class="width1" for="birthday"><span class="form_bg">Fecha de nacimiento:</span></label>
        <div>
          <select class="signup_select width2" id="birth_day" name="birth_day">
            <option disabled selected>D&iacute;a</option>
            <?php
              for ($i = 1; $i <= 31; $i++)
                echo "<option value='$i'>$i</option>";
            ?>
          </select>
          <select class="signup_select width2" id="birth_month" name="birth_month">
            <option disabled selected>Mes</option>
            <?php
              for ($i = 1; $i <= 12; $i++)
                echo "<option value='$i'>$i</option>";
            ?>
          </select>
          <select class="signup_select width3" id="birth_year" name="birth_year">
            <option disabled selected>A&ntilde;o</option>
            <?php
              for ($i = 1900; $i <= date('Y'); $i++)
                echo "<option value='$i'>$i</option>";
            ?>
          </select>
        </div>
        <br/>
        <label class="width1" for="credit_card"><span class="form_bg">Tarjeta de cr&eacute;dito:</span></label>
        <input type="text" name="credit_card" placeholder="Tarjeta de cr&eacute;dito" id="credit_card" required/>
        <br/>
        <label class="width1" for="card_expiration"><span class="form_bg">Caduca en:</span></label>
        <div>
          <select class="signup_select width2" id="card_month" name="card_month">
            <option disabled selected>Mes</option>
            <?php
              for ($i = 1; $i <= 12; $i++)
                echo "<option value='$i'>$i</option>";
            ?>
          </select>
          <select class="signup_select width3" id="card_year" name="card_year">
            <option disabled selected>A&ntilde;o</option>
            <?php
              for ($i = date('Y'); $i <= (date('Y') + 50); $i++)
                echo "<option value='$i'>$i</option>";
            ?>
          </select>
        </div>
        <br/>
        <label class="width1" for="user"><span class="form_bg">Usuario:</span></label>
        <input type="text" name="user" placeholder="Usuario" id="user" required/>
        <br/>
        <label class="width1" for="password"><span class="form_bg">Contrase&ntilde;a:</span></label>
        <input type="password" name="password" placeholder="Contrase&ntilde;a" id="password" required/>
        <br/>
        <label class="width1" for="password2"><span class="form_bg">Repita la contrase&ntilde;a:</span></label>
        <input type="password" name="password2" placeholder="Contrase&ntilde;a" id="password2" required/>
        <br/>
        <input class="black_button" type="button" onclick="submit_signup()" value="Confirmar" id="sign_up_button"/>
      </fieldset>

    </form>

  </div>

  <footer>
    <p id="footer_text">&copy;2015 BETSY apuestas deportivas - Escuela Polit&eacute;cnica Superior UAM</p>
  </footer>

</body>

<script type="text/javascript">
  $(function() {
    $("form input").keypress(function(e) {
      if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
        if ($("#password_field").is(":focus") || $("#user_field").is(":focus")) {
          $("#login_button").click();
          return false;
        }
        return true;
      } else
        return true;
    });
  });
</script>

</html>
