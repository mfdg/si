<?php

/*-- - - - - - - - - - - - - - - - - - - - - -
  -
  -    registration.php
  -
  -  Fichero que se encarga de guardar la
  -  informacion de un nuevo usuario
  -  registrado
  -
  -  Autores: Mateo Fdez de Gamboa Santiuste
  -           Pinar Sanz Sacristan
  -
  - - - - - - - - - - - - - - - - - - - - - -*/

  session_start();
  $user = $_REQUEST['user'];
  if (file_exists("usuarios/".$user) && is_dir("usuarios/".$user)) {
    session_start();
    $_SESSION['error'] = "El nombre de usuario escogido ya está en uso";
    $_SESSION['return'] = "sign_up.php";
    header("Location: error.php");
    exit();
  }

  $_SESSION['user'] = $user;
  $_SESSION['password'] = md5($_REQUEST['password']);
  $_SESSION['name'] = ucfirst($_REQUEST['name']);
  $_SESSION['surname'] = ucfirst($_REQUEST['surname']);
  $_SESSION['birthday'] = $_REQUEST['birth_day']."/".$_REQUEST['birth_month']."/".$_REQUEST['birth_year'];
  $_SESSION['mail'] = $_REQUEST['mail'];
  $_SESSION['credit_card'] = $_REQUEST['credit_card'];
  $_SESSION['credit_card_balance'] = "0";
  $_SESSION['card_expiration'] = $_REQUEST['card_month']."/".$_REQUEST['card_year'];

  mkdir('usuarios/'.$_REQUEST['user']);

  $file = fopen('usuarios/'.$_REQUEST['user'].'/datos.dat', 'w+');
  fwrite($file, $_SESSION['user']."\n".$_SESSION['password']."\n".$_SESSION['name']."\n".$_SESSION['surname']."\n".$_SESSION['birthday']."\n".$_SESSION['mail']."\n".$_SESSION['credit_card']."\n".$_SESSION['credit_card_balance']."\n".$_SESSION['card_expiration']);
  fclose($file);

  $hfile = fopen('usuarios/'.$_REQUEST['user'].'/history.xml', 'w+');
  fwrite($hfile, "<?xml version='1.0' encoding='utf-8'?>\n<history></history>");
  fclose($hfile);

  header("Location: index.php");
  exit();
?>