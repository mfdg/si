<?php

/*-- - - - - - - - - - - - - - - - - - - - - -
  -
  -    add_money.php
  -
  -  Fichero que se encarga de guardar las
  -  actualizaciones de saldo del usuario
  -
  -  Autores: Mateo Fdez de Gamboa Santiuste
  -           Pinar Sanz Sacristan
  -
  - - - - - - - - - - - - - - - - - - - - - -*/

  session_start();
  $_SESSION['credit_card_balance'] = $_SESSION['credit_card_balance'] + $_REQUEST['amount'];

  //if (file_exists("usuarios/".$_SESSION['user']) && is_dir("usuarios/".$_SESSION['user'])) {
    $file = fopen("usuarios/".$_SESSION['user']."/datos.dat", 'w');
    fwrite($file, $_SESSION['user']."\n".$_SESSION['password']."\n".$_SESSION['name']."\n".$_SESSION['surname']."\n".$_SESSION['birthday']."\n".$_SESSION['mail']."\n".$_SESSION['credit_card']."\n".$_SESSION['credit_card_balance']."\n".$_SESSION['card_expiration']);
    fclose($file);

    header("Location: profile.php");
  /*} /*else { // ERROR
    exit();
  */
?>