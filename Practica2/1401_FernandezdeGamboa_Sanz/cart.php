<!DOCTYPE html>

<!-- - - - - - - - - - - - - - - - - - - - - -
  -
  -    cart.php
  -
  -  Pagina donde se consulta el carrito
  -
  -  Autores: Mateo Fdez de Gamboa Santiuste
  -           Pinar Sanz Sacristan
  -
  - - - - - - - - - - - - - - - - - - - - - -->

<html>
<head>
  <title>BETSY apuestas</title>
  <link rel="stylesheet" type="text/css" href="styles.css"/>
  <script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js"></script>
  <script type="text/javascript" src="behavior.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js" ></script>
  <meta charset="UTF-8">

  <link rel="shortcut icon" type="image/x-ico" href="img/favicon.ico"/>
</head>

<body>
  <header>
    <img id="betsy_girl" alt="girl" src="img/betsy_girl.png"/>
    <a href="index.php"><p id="betsy_title">BETSY</p></a>

    <?php
      session_start();
      if (isset($_SESSION['user'])) {
        setcookie('user', $_SESSION['user']);
    ?>

      <div id="logged_message">
        <p>Bienvenido, <a class="link" href="profile.php"><b><?php echo $_SESSION['name']; ?></b></a></p>
        <p>
          <a class="white_button" href="logout.php">Cerrar sesi&oacute;n</a>
        </p>
      </div>

    <?php
      } else {
    ?>

      <form name="datos" id="login">
        <?php
        if (isset($_COOKIE['user']))
          echo '<input id="user_field" type="text" name="user" value="'.$_COOKIE['user'].'" required>';
        else
          echo '<input id="user_field" type="text" name="user" placeholder="Usuario" required>';
        ?>
        <input id="password_field" type="password" name="password" placeholder="Contrase&ntilde;a" required><br/>
        <div id="error_login">Error en usuario o contrase&ntilde;a</div>
        <div id="reminder_login">
          &iquest;A&uacute;n no tienes cuenta? <a class="link" href="sign_up.php">&iexcl;Reg&iacute;strate!</a>
        </div>
        <input id="login_button" class="white_button" type="button" value="Entrar" onclick="send_login()">
      </form>

    <?php
      }
    ?>

  <a href="cart.php" id="betsy_cart" class="cart_icon" title="Cart"><p id="betsy_cart_counter"><?php 
                                                        if (isset($_SESSION['bets']))
                                                          echo count($_SESSION['bets']);
                                                        else echo '0';
                                                      ?></p></a>

  </header>


  <div id="lateral_menu">
    <hr/>
    <div <?php if (!strcmp($get_sport, '')) echo "class='menu_selected'"; ?>><a href="index.php">
    <img class="icon" alt="home" src="img/home.png"/>Inicio</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'baloncesto')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=baloncesto">
    <img class="icon" alt="basket" src="img/basket.png"/>Baloncesto</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'ciclismo')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=ciclismo">
    <img class="icon" alt="bike" src="img/bike.png"/>Ciclismo</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'formula1')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=formula1">
    <img class="icon" alt="formula1" src="img/formula1.png"/>F&oacute;rmula 1</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'futbol')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=futbol">
    <img class="icon" alt="football" src="img/football.png"/>F&uacute;tbol</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'golf')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=golf">
    <img class="icon" alt="golf" src="img/golf.png"/>Golf</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'hockey')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=hockey">
    <img class="icon" alt="hockey" src="img/hockey.png"/>Hockey</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'rugby')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=rugby">
    <img class="icon" alt="rugby" src="img/rugby.png"/>Rugby</a></div>
    <hr/>
    <div <?php if (!strcmp($get_sport, 'tenis')) echo "class='menu_selected'"; ?>><a href="index.php?deporte=tenis">
    <img class="icon" alt="tennis" src="img/tennis.png"/>Tenis</a></div>
    <hr/>
  </div>


  <div id="main">

  <?php
    $our_bet = "";
    $flag = 0;
    $tie = 1;
    $index = 0;
    $xml = simplexml_load_file("bets.xml") or die("Error: Cannot create object");

    if (!isset($_SESSION['bets']) || count($_SESSION['bets']) == 0) {
  ?>

    <div>
      <h2>&iexcl;Tu carrito de apuestas est&aacute; vac&iacute;o!</h2>
      <p>A&ntilde;ade tu primera <b><a href="index.php" class="link2">apuesta</a></b> desde la pantalla principal y &iexcl;comienza a ganar dinero con nosotros!</p>
    </div>

  <?php
    } else {
      $comp = array();
      $match = array();
      foreach ($_SESSION['bets'] as $cbet) {
        $id = $cbet[0];
        $amount = $cbet[1];
        $for = $cbet[2];
        foreach ($xml->children() as $sport) {
          foreach($sport->children() as $bet) {
            if (!strcmp($bet["id"], $id)) {
              $flag = 1;
              $our_bet = $bet;
              break;
            }
          }
          if (strcmp($our_bet, "")) {
            $our_sport = $sport;
            if (!strcmp($our_sport->getName(), "baloncesto") || !strcmp($our_sport->getName(), "tenis"))
              $tie = 0;
            break;
          }
        }

        if ($flag) {
          $j = 0;
          foreach ($our_bet->children() as $key) {
            if (strpos($key->getName(),'#') === false) {
              if (!strcmp($key->getName(), "rates")) {
                $i = 0;
                foreach ($key->children() as $rates) {
                  if (strpos($rates->getName(),'#') === false) {
                    $rate[$i] = (string)$rates; // Rates
                    $i++;
                  }
                }
              } else if (!strcmp($key->getName(), "date")) {
                $date = (string)$key; // Date
              } else {
                if (!strcmp($our_sport->getName(), "baloncesto") || !strcmp($our_sport->getName(), "futbol") ||
                    !strcmp($our_sport->getName(), "hockey") || !strcmp($our_sport->getName(), "rugby") ||
                    !strcmp($our_sport->getName(), "tenis")) {
                  $is_comp = 0;
                  $team[$j] = (string)$key;
                  $j++;
                } else if (!strcmp($our_sport->getName(), "ciclismo") ||
                           !strcmp($our_sport->getName(), "formula1") ||
                           !strcmp($our_sport->getName(), "golf")) {
                  $is_comp = 1;
                  $title = (string)$key;
                }

              }
            }
          }

          if ($is_comp) {
            $row = "<tr>
              <td>$title</td>
              <td>$rate[$for]</td>
              <td id='$index'>$amount &euro;</td>
              <td>$date</td>
              <td class='border_right'>
                <a href='javascript:edit_bet($index);'><img class='icon' alt='edit' src='img/edit.png'/></a>
                <a href='remove_bet.php?index=$index'><img class='icon' alt='cancel' src='img/cancel.png'/></a></td></tr>";
            array_push($comp, $row);
          } else {
            if ($for == 0)
              $winner = '1';
            else if ($for == 1)
              if (tie == 1)
                $winner = 'X';
              else
                $winner = '2';
            else
              $winner = '2';

            $row = "<tr>
              <td>$team[0]</td>
              <td>$team[1]</td>
              <td>$winner</td>
              <td>$rate[$for]</td>
              <td id='$index'>$amount &euro;</td>
              <td>$date</td>
              <td class='border_right'>
                <a id='accept$index' href='javascript:accept_edition($index);' hidden><img class='icon' alt='edit' src='img/accept.png'/></a>
                <a id='edit$index' href='javascript:edit_bet($index);'><img class='icon' alt='edit' src='img/edit.png'/></a>
                <a id='cancel$index' href='javascript:cancel_edition($index);' hidden><img class='icon' alt='cancel' src='img/cancel.png'/></a>
                <a id='remove$index' href='remove_bet.php?index=$index'><img class='icon' alt='cancel' src='img/cancel.png'/></a></td></tr>";
            array_push($match, $row);
          }
          $our_bet = "";
          $flag = 0;
        }
        $tie = 1;
        $index++;
      }

      echo "<h1 class='cart_title'>Apuestas realizadas</h1>";

      if (count($match) != 0) {
  ?>

    <table class="bordered">
      
      <thead>
        <tr>
          <th colspan="2" class="ul_corner">Partido</th>
          <th>Apuesta</th>
          <th>Cuota</th>
          <th>Dinero</th>
          <th>Fecha</th>
          <th class="ur_corner border_right"></th>
        </tr>
      </thead>

      <tbody>
      <?php
        foreach ($match as $bet) {
          echo $bet;
        }
      ?>
        <tr>
        </tr>

      </tbody>
    </table>

    <?php
      }

      if (count($comp) != 0) {
    ?>

    <table class="bordered">
      
      <thead>
        <tr>
          <th class="ul_corner">Competici&oacute;n</th>
          <th>Cuota</th>
          <th>Dinero</th>
          <th>Fecha</th>
          <th class="ur_corner border_right"></th>
        </tr>
      </thead>

      <tbody>
      
      <?php
        foreach ($comp as $bet) {
          echo $bet;
        }
      ?>
        <tr>
        </tr>

      </tbody>
    </table><br/>

    <?php
      }
    ?>

    <div id="cart_buttons">
      <button class="black_button" onclick="cancel_cart()">Cancelar</button>
      <button class="black_button" onclick="location.href='index.php'">Apostar m&aacute;s</button>
      <button class="black_button" onclick="accept_cart(<?php if (isset($_SESSION['user']))
                                                                echo 'true, '.$_SESSION['credit_card_balance'].', '.$_SESSION['to_pay'];
                                                              else
                                                                echo 'false, 0, 1'; ?>)">Confirmar</button>
    </div>

    <?php
      }
    ?>

  </div>

  <footer>
    <p id="footer_text">&copy;2015 BETSY apuestas deportivas - Escuela Polit&eacute;cnica Superior UAM</p>
  </footer>

</body>

<script type="text/javascript">
  $(function() {
    $("form input").keypress(function(e) {
      if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
        if ($("#password_field").is(":focus") || $("#user_field").is(":focus")) {
          $("#login_button").click();
          return false;
        }
        return true;
      } else
        return true;
    });
  });
</script>

</html>